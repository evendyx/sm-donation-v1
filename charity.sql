-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 15 Feb 2020 pada 10.05
-- Versi server: 10.4.6-MariaDB
-- Versi PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `charity`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `advertisers`
--

CREATE TABLE `advertisers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `Location` varchar(299) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'home',
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'image',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `displayname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT 0,
  `order` int(11) NOT NULL DEFAULT 0,
  `title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `order`, `title`, `slug`, `color`, `created_at`, `updated_at`) VALUES
(2, 0, 2, 'Human Sociaty', 'Categores', 'primary', '2019-04-20 16:12:28', '2020-02-15 01:21:20'),
(3, 0, 2, 'Sports and Fun', 'Categores2', 'warning', '2019-04-20 16:12:55', '2020-02-15 01:21:56'),
(4, 0, 5, 'Categores', 'Cause2', 'success', '2019-04-20 16:13:06', '2019-04-20 16:13:06');

-- --------------------------------------------------------

--
-- Struktur dari tabel `causes`
--

CREATE TABLE `causes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `Title_en` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Title_ar` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Title_gr` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Content_en` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Content_ar` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Content_gr` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Raised` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Goal` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Donors` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `causes`
--

INSERT INTO `causes` (`id`, `Title_en`, `Title_ar`, `Title_gr`, `slug`, `Content_en`, `Content_ar`, `Content_gr`, `category_id`, `Raised`, `Goal`, `Donors`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Lorem Ipsum', NULL, NULL, 'lala-c', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.', NULL, NULL, '2', 'Rp 25.000.000', 'Rp 15.000.000', '1', 'storage/Causes/February2020/mni.png', '2020-02-15 01:07:19', '2020-02-15 01:07:19'),
(2, 'Lorem Ipsum', NULL, NULL, 'lala-d', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.\r\n\r\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.', NULL, NULL, '2', 'Rp 25.000.000', 'Rp 50.000.000', '500', 'storage/Causes/February2020/mni.png', '2020-02-15 01:09:52', '2020-02-15 01:09:52'),
(3, 'Lorem Ipsum', NULL, NULL, 'lala-e', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.\r\n\r\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.', NULL, NULL, '2', 'Rp 25.000.000', 'Rp 35.000.000', '450', 'storage/Causes/February2020/mni.png', '2020-02-15 01:10:34', '2020-02-15 01:10:34');

-- --------------------------------------------------------

--
-- Struktur dari tabel `comments`
--

CREATE TABLE `comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `Post_id` int(11) NOT NULL DEFAULT 1,
  `User_id` int(11) NOT NULL DEFAULT 1,
  `Comment` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `events`
--

CREATE TABLE `events` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `Title_en` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Title_ar` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Title_gr` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Content_en` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Content_ar` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Content_gr` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Days` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Hours` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Minutes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `FinishTime` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `StartTime` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `events`
--

INSERT INTO `events` (`id`, `Title_en`, `Title_ar`, `Title_gr`, `slug`, `Content_en`, `Content_ar`, `Content_gr`, `image`, `Days`, `Hours`, `Minutes`, `Address`, `FinishTime`, `StartTime`, `created_at`, `updated_at`) VALUES
(1, 'Lorem Ipsum is simply dummy text', NULL, NULL, 'lala-a', '<!-- ================================ links Events Content Start ========================================================================= -->\r\n                <!-- ================================ links Events Content Start ========================================================================= -->\r\n                <!-- ================================ links Events Content Start ========================================================================= -->\r\n                <!-- ================================ links Events Content Start ========================================================================= -->\r\n                <!-- ================================ links Events Content Start ========================================================================= -->\r\n                <!-- ================================ links Events Content Start ========================================================================= -->\r\n                What is Lorem Ipsum?\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n              <!-- ================================ links Events Content Start ========================================================================= -->\r\n              <!-- ================================ links Events Content Start ========================================================================= -->\r\n              <!-- ================================ links Events Content Start ========================================================================= -->\r\n              <!-- ================================ links Events Content Start ========================================================================= -->\r\n              <!-- ================================ links Events Content Start ========================================================================= -->\r\n              <!-- ================================ links Events Content Start ========================================================================= -->', NULL, NULL, 'storage/Events/February2020/Ignite-Event-Banner-1.png', '07', '05', '2020', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', '13:00', '08:00', '2020-02-14 01:54:59', '2020-02-14 02:09:33'),
(2, 'Lorem Ipsum is simply dummy text', NULL, NULL, 'lala-b', '<!-- ================================ links Events Content Start ========================================================================= -->\r\n                Why do we use it?\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n              <!-- ================================ links Events Content Start ========================================================================= -->', NULL, NULL, 'storage/Events/February2020/Ignite-Event-Banner-1.png', '09', '09', '2020', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', '13:00', '08:00', '2020-02-14 02:08:35', '2020-02-14 02:09:40');

-- --------------------------------------------------------

--
-- Struktur dari tabel `galleries`
--

CREATE TABLE `galleries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `galleries`
--

INSERT INTO `galleries` (`id`, `image`, `created_at`, `updated_at`) VALUES
(1, 'storage/Galleres/February2020/1.png', '2020-02-13 21:13:35', '2020-02-13 21:13:35'),
(2, 'storage/Galleres/February2020/2.png', '2020-02-13 21:13:43', '2020-02-13 21:13:43'),
(3, 'storage/Galleres/February2020/3.png', '2020-02-13 21:13:51', '2020-02-13 21:13:51'),
(4, 'storage/Galleres/February2020/2.png', '2020-02-13 21:20:31', '2020-02-13 21:20:31'),
(5, 'storage/Galleres/February2020/3.png', '2020-02-13 21:20:51', '2020-02-13 21:20:51'),
(6, 'storage/Galleres/February2020/1.png', '2020-02-13 21:21:17', '2020-02-13 21:21:17'),
(7, 'storage/Galleres/February2020/4.png', '2020-02-13 21:23:41', '2020-02-13 21:23:41'),
(8, 'storage/Galleres/February2020/5.png', '2020-02-13 21:23:48', '2020-02-13 21:23:48');

-- --------------------------------------------------------

--
-- Struktur dari tabel `menus`
--

CREATE TABLE `menus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'main-menu', '2019-04-13 14:55:31', '2019-04-13 14:55:31'),
(2, 'main-menu-gr', '2019-04-17 14:38:56', '2019-04-17 14:38:56'),
(3, 'main-menu-ar', '2019-04-24 08:25:22', '2019-04-24 08:25:22');

-- --------------------------------------------------------

--
-- Struktur dari tabel `menu_items`
--

CREATE TABLE `menu_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `menu_id` int(11) NOT NULL DEFAULT 0,
  `order` int(11) NOT NULL DEFAULT 0,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `order`, `title`, `url`, `target`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Home', '/', '_blank', '2019-04-17 13:22:26', '2019-04-17 13:22:26'),
(2, 1, 2, 'about', 'http://localhost:8000/about', '_blank', '2019-04-21 08:32:05', '2019-04-21 08:32:05'),
(3, 1, 3, 'Causes', 'http://localhost:8000/Causes', '_blank', '2019-04-17 13:23:02', '2019-04-17 13:23:02'),
(4, 1, 4, 'Event', 'http://localhost:8000/Events', '_blank', '2019-04-17 13:23:18', '2019-04-17 13:23:18'),
(5, 1, 5, 'Posts', 'http://localhost:8000/Posts', '_blank', '2019-04-17 13:24:03', '2019-04-17 13:24:03'),
(8, 1, 6, 'Contact Us', 'http://localhost:8000/Contact', '_blank', '2019-04-17 14:40:54', '2019-04-17 14:40:54'),
(9, 2, 1, 'ZUHAUSE', '/', '_self', '2019-04-24 08:26:45', '2019-04-24 08:26:45'),
(10, 2, 2, 'Über', 'http://localhost:8000/about', '_blank', '2019-04-24 08:27:14', '2019-04-24 08:27:14'),
(11, 2, 3, 'Ursachen', 'http://localhost:8000/Causes', '_blank', '2019-04-24 08:30:34', '2019-04-24 08:30:34'),
(12, 2, 4, 'Veranstaltungen', 'http://localhost:8000/Events', '_blank', '2019-04-24 08:31:26', '2019-04-24 08:31:26'),
(13, 2, 5, 'Beiträge', 'http://localhost:8000/Posts', '_self', '2019-04-24 08:32:01', '2019-04-24 08:32:01'),
(14, 2, 6, 'Kontaktiere uns', 'http://localhost:8000/Contact', '_self', '2019-04-24 08:33:37', '2019-04-24 08:33:37'),
(15, 3, 1, 'الرئيسيه', '/', '_blank', '2019-04-24 08:34:28', '2019-04-24 08:34:28'),
(16, 3, 2, 'معلومات عنا', 'http://localhost:8000/about', '_blank', '2019-04-24 10:55:49', '2019-04-24 10:55:49'),
(17, 3, 3, 'الأسباب', 'http://localhost:8000/Causes', '_blank', '2019-04-24 10:56:19', '2019-04-24 10:56:19'),
(18, 3, 4, 'أحداث', 'http://localhost:8000/Events', '_blank', '2019-04-24 10:56:41', '2019-04-24 10:56:41'),
(19, 3, 5, 'المشاركات', 'http://localhost:8000/Posts', '_blank', '2019-04-24 10:57:07', '2019-04-24 10:57:07'),
(21, 3, 6, 'اتصل بنا', 'http://localhost:8000/Contact', '_blank', '2019-04-24 10:57:54', '2019-04-24 10:57:54');

-- --------------------------------------------------------

--
-- Struktur dari tabel `messages`
--

CREATE TABLE `messages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `Content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_04_06_221802_create_roles_table', 2),
(4, '2014_10_12_000000_create_users_table', 3),
(5, '2019_04_08_124117_create_posts_table', 4),
(6, '2019_04_08_130832_create_categories_table', 5),
(7, '2019_04_08_131509_create_posts_table', 5),
(8, '2019_04_08_132134_create_menus_table', 6),
(9, '2019_04_08_132745_create_menu_items_table', 7),
(10, '2019_04_08_140014_create_advertisers_table', 8),
(11, '2019_04_08_141156_create_advertisers_table', 9),
(12, '2019_04_08_144316_create_settings_table', 10),
(13, '2019_04_08_180227_create_settings_table', 11),
(14, '2019_04_18_124656_create_settings_table', 12),
(15, '2019_04_20_110544_create_causes_table', 13),
(16, '2019_04_20_111522_create_events_table', 13),
(17, '2019_04_20_112859_create_galleries_table', 13),
(18, '2019_04_20_113049_create_messages_table', 13),
(19, '2019_04_20_120030_create_posts_table', 14),
(20, '2019_04_23_103420_create_comments_table', 15);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL DEFAULT 1,
  `category_id` int(11) NOT NULL DEFAULT 0,
  `Title_en` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Title_ar` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Title_gr` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body_en` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body_ar` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body_gr` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_title` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `featured` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `posts`
--

INSERT INTO `posts` (`id`, `author_id`, `category_id`, `Title_en`, `Title_ar`, `Title_gr`, `body_en`, `body_ar`, `body_gr`, `slug`, `seo_title`, `excerpt`, `image`, `meta_description`, `meta_keywords`, `featured`, `created_at`, `updated_at`) VALUES
(4, 16, 3, 'RUN THE RUNWAY 2020', 'القلب إلى القلب', 'RUN THE RUNWAY 2019', '<p>At SickKids, we try our best to help patients and families feel as welcome and comfortable as they would at home. Donations of brand new toys, electronics, crafts, books, and games help us bring joy and comfort to children throughout their hospital stay or appointment. Donations may be used in our playrooms, as rewards after completing a test or undergoing a scary procedure, and/or as gift for a patient&rsquo;s birthday or other special milestone.Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Idque Caesaris facere volun tate liceret: sese habere. Magna pars studiorum, prodita quaerimus. Magna pars studiorum, prodita quaerimus. Fabio vel iudice vincam, sunt in culpa&hellip;</p>', 'لكن sickkids، ونحن نبذل قصارى جهدنا لمساعدة المرضى وعائلاتهم يشعرون كما موضع ترحيب ومريحة كما يفعلون في المنزل. تبرعات من العلامة التجارية الجديدة اللعب، والإلكترونيات، والحرف، والكتب، والألعاب تساعدنا في جلب الفرح والراحة للأطفال في جميع أنحاء الإقامة في المستشفى أو التعيين. ويمكن استخدام التبرعات في قاعات اللعب لدينا، كمكافأة بعد الانتهاء من الاختبار أو خضوعه لإجراء مخيف، و / أو كهدايا لعيد ميلاد المريض أو غيرها من milestone.Lorem خاصة الجزر جدا، وأسعار الطماطم adipisici، ولكن مثل هذا الوقت أنها تحدث في العمل والطريق من السمنة. وسيسمح اقع أن نفعل ذلك مع قيصر انه لن يكون عليه. وهناك جزء كبير من دراساتنا، وتنتج تسعى إلى معرفة. وهناك جزء كبير من دراساتنا، وتنتج تسعى إلى معرفة. فابيان أو قاض للتغلب، في خطأ ...', 'At SickKids, we try our best to help patients and families feel as welcome and comfortable as they would at home. Donations of brand new toys, electronics, crafts, books, and games help us bring joy and comfort to children throughout their hospital stay or appointment. Donations may be used in our playrooms, as rewards after completing a test or undergoing a scary procedure, and/or as gift for a patient’s birthday or other special milestone.Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Idque Caesaris facere volun tate liceret: sese habere. Magna pars studiorum, prodita quaerimus. Magna pars studiorum, prodita quaerimus. Fabio vel iudice vincam, sunt in culpa…', 'RUN-THE-RUNWAY-2020', 'RUN THE RUNWAY 2020', NULL, 'storage/posts/February2020/action.png', NULL, NULL, NULL, '2019-04-22 17:13:43', '2020-02-15 00:59:17'),
(5, 16, 3, 'HOLIDAY GIFTS IN KIND', 'حتى الزناد مكتب يختلف رعاية صحية مجانية', 'HOLIDAY GIFTS IN KIND', '<p>At SickKids, we try our best to help patients and families feel as welcome and comfortable as they would at home. Donations of brand new toys, electronics, crafts, books, and games help us bring joy and comfort to children throughout their hospital stay or appointment. Donations may be used in our playrooms, as rewards after completing a test or undergoing a scary procedure, and/or as gift for a patient&rsquo;s birthday or other special milestone.Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Idque Caesaris facere volun tate liceret: sese habere. Magna pars studiorum, prodita quaerimus. Magna pars studiorum, prodita quaerimus. Fabio vel iudice vincam, sunt in culpa&hellip;</p>', 'لكن sickkids، ونحن نبذل قصارى جهدنا لمساعدة المرضى وعائلاتهم يشعرون كما موضع ترحيب ومريحة كما يفعلون في المنزل. تبرعات من العلامة التجارية الجديدة اللعب، والإلكترونيات، والحرف، والكتب، والألعاب تساعدنا في جلب الفرح والراحة للأطفال في جميع أنحاء الإقامة في المستشفى أو التعيين. ويمكن استخدام التبرعات في قاعات اللعب لدينا، كمكافأة بعد الانتهاء من الاختبار أو خضوعه لإجراء مخيف، و / أو كهدايا لعيد ميلاد المريض أو غيرها من milestone.Lorem خاصة الجزر جدا، وأسعار الطماطم adipisici، ولكن مثل هذا الوقت أنها تحدث في العمل والطريق من السمنة. وسيسمح اقع أن نفعل ذلك مع قيصر انه لن يكون عليه. وهناك جزء كبير من دراساتنا، وتنتج تسعى إلى معرفة. وهناك جزء كبير من دراساتنا، وتنتج تسعى إلى معرفة. فابيان أو قاض للتغلب، في خطأ ...', 'At SickKids, we try our best to help patients and families feel as welcome and comfortable as they would at home. Donations of brand new toys, electronics, crafts, books, and games help us bring joy and comfort to children throughout their hospital stay or appointment. Donations may be used in our playrooms, as rewards after completing a test or undergoing a scary procedure, and/or as gift for a patient’s birthday or other special milestone.Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Idque Caesaris facere volun tate liceret: sese habere. Magna pars studiorum, prodita quaerimus. Magna pars studiorum, prodita quaerimus. Fabio vel iudice vincam, sunt in culpa…', 'HOLIDAY-GIFTS-KIND', 'RUN THE RUNWAY 2020', NULL, 'storage/posts/February2020/holiday-gift-.png', NULL, NULL, NULL, '2019-04-22 17:16:06', '2020-02-15 00:59:27');

-- --------------------------------------------------------

--
-- Struktur dari tabel `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'adminstartor', '2019-04-20 15:45:42', '2019-04-20 15:45:42'),
(4, 'User', 'Normal User', '2019-04-20 15:57:08', '2019-04-20 15:57:08');

-- --------------------------------------------------------

--
-- Struktur dari tabel `settings`
--

CREATE TABLE `settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `Language` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'english',
  `SiteTitle` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Name` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Googlemap` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PhoneNumber` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LogoPicture` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `HomePicture` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `AboutPicture` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MetaDescription` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MetaKeyWords` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `GitHub` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Slack` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Dribbble` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LinkedIn` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Behance` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Digg` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Flickr` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Vimeo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url_App` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `FaviconOne` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `FaviconTwo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `FaviconThree` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_en` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_gr` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_ar` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_home_en` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_home_gr` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_home_ar` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_title_home_en` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_title_home_gr` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_title_home_ar` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_about_en` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_about_gr` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_about_ar` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_about_en` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_about_gr` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_about_ar` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_blog_en` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_blog_gr` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_blog_ar` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_feature_en` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_feature_gr` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_feature_ar` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_feature_two_en` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_feature_two_gr` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_feature_two_ar` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_feature_three_en` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_feature_three_gr` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_feature_three_ar` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `settings`
--

INSERT INTO `settings` (`id`, `Language`, `SiteTitle`, `Name`, `Location`, `Googlemap`, `video`, `PhoneNumber`, `Email`, `LogoPicture`, `HomePicture`, `AboutPicture`, `MetaDescription`, `MetaKeyWords`, `Facebook`, `Twitter`, `GitHub`, `Slack`, `Dribbble`, `LinkedIn`, `Behance`, `Digg`, `Flickr`, `Vimeo`, `youtube`, `url_App`, `FaviconOne`, `FaviconTwo`, `FaviconThree`, `about_en`, `about_gr`, `about_ar`, `title_home_en`, `title_home_gr`, `title_home_ar`, `sub_title_home_en`, `sub_title_home_gr`, `sub_title_home_ar`, `title_about_en`, `title_about_gr`, `title_about_ar`, `content_about_en`, `content_about_gr`, `content_about_ar`, `content_blog_en`, `content_blog_gr`, `content_blog_ar`, `content_feature_en`, `content_feature_gr`, `content_feature_ar`, `content_feature_two_en`, `content_feature_two_gr`, `content_feature_two_ar`, `content_feature_three_en`, `content_feature_three_gr`, `content_feature_three_ar`, `created_at`, `updated_at`) VALUES
(1, 'English', 'BeGood Man', 'BeGood Man', 'Jalan Ring Road Utara, Sanggrahan, Kaliwaru, Condongcatur, Kec. Depok, Kab. Sleman, Daerah Istimewa Yogyakarta', 'https://www.google.co.id/maps/place/Hartono+Mall+Yogyakarta/@-7.7607536,110.4046253,15z/data=!4m5!3m4!1s0x2e7a59a17e4e49eb:0xa864a4f7fe95e090!8m2!3d-7.7596294!4d110.398508', 'https://youtu.be/KKhFWS6OCk4', '082136607128', 'admin@admin.com', 'storage/settings/February2020/logotext.png', 'storage/settings/February2020/2.png', 'storage/settings/February2020/6.png', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old', 'settings', 'https://www.facebook.com/', 'settings', 'settings', 'setting', 'settings', 'settings', 'settings', 'settings', 'settings', 'settings', 'settings', 'settings', 'storage/settings/February2020/favicon-32x32.png', 'storage/settings/February2020/favicon-96x96.png', 'storage/settings/February2020/favicon-16x16.png', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 'Möchten Sie noch erfolgreicher sein? Lernen Sie, Lernen und Wachstum zu lieben. Je mehr Aufwand Sie in die Verbesserung Ihrer Fähigkeiten investieren, desto höher ist der Gewinn, den Sie erzielen. Stellen Sie fest, dass die Dinge zunächst hart sein werden, aber die Belohnungen werden es wert sein.', 'هل تريد أن تكون أكثر نجاحا؟ تعلم أن تحب التعلم والنمو. كلما بذلت المزيد من الجهود لتحسين مهاراتك ، زادت المكاسب التي ستحصل عليها. أدرك أن الأمور ستكون صعبة في البداية ، ولكن المكافآت ستكون تستحق العناء.', 'What is Lorem Ipsum?', 'Für die Menschen und die Gründe, die Ihnen', 'للناس ويسبب لك الرعاية', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'schöne Dinge in der Welt', 'أشياء جميلة في العالم', 'Lorem Ipsum is simply dummy text', 'Büros in Frankreich, Norwegen und Schweden eröffnet', 'مكاتب افتتحت في فرنسا والنرويج والسويد', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum praesentium officia, fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur aspernatur at, eaque hic repellendus sit dicta consequatur quae, ut harum ipsam molestias maxime non nisi reiciendis eligendi! Doloremque quia pariatur harum ea amet quibusdam quisquam.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Es stellt Dienstleistungen, sie für jede Outdoor-Erfahrung akzeptiert läuft? Aber lehnt Ergebnis, und das nennt man die Folgen vermieden werden, da diese nicht nur die lästige Spuck Wahl! Da es sich um eine Resultierende dieser Welt ist in einigen der Schmerz zu lieben war ein stehender Mann.', 'أبجد هوز دولور الجلوس امات، consectetur adipiscing إيليت. وهو يقدم الخدمات، وقالت انها تدير المقبولة للأي خبرة في الهواء الطلق؟ ولكن ترفض النتيجة، وهذا ما يسمى يتم تجنب العواقب، وهذه ليست فقط في الانتخابات ترفض الأكثر اضطرابا! لأنه الناتجة من هذا العالم إلى الحب في بعض من الألم كان رجلا واقفا.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 'Geöffnete fünfte mitte geteilte große Fliege, die nicht reichlich lebt. Der Abend scheint zu sagen, dass Gott der Gerechte gegeben wird. Sechste Tage des Dritten Geistes bewässern die Meere. Genannt kann nicht sein Dritter. Abend danach. Alle Stars. Trockne die dritten Tage', 'فتح الخامس وسط تقسيم ذبابة كبيرة تجمع العيش في عمق لا بوفرة. يبدو المساء قائلاً إن الله تعالى يعطى أياماً سادسة من روح البحر الثالثة. دعا لا يمكن له الثالثة. المساء على. كل النجوم. مواسم جافة للأيام الثالثة', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 'Multiplizieren ist die Herrschaft der Lichtherrschaft, die inmitten eines Lebens gesetzt wird.', 'تتضاعف السيطرة على ضوء السيادة التي تُعطى وسط لقمة العيش أقوم بإحضار كل إحضار لها أيضًا.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 'Multiplizieren ist die Herrschaft der Lichtherrschaft, die inmitten eines Lebens gesetzt wird.', 'تتضاعف السيطرة على ضوء السيادة التي تُعطى وسط لقمة العيش أقوم بإحضار كل إحضار لها أيضًا.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 'Multiplizieren ist die Herrschaft der Lichtherrschaft, die inmitten eines Lebens gesetzt wird.', 'تتضاعف السيطرة على ضوء السيادة التي تُعطى وسط لقمة العيش أقوم بإحضار كل إحضار لها أيضًا.', NULL, '2020-02-15 01:23:30');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT 0,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'storage/users/default.png',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `email_verified_at`, `avatar`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(16, 1, 'Administrator', 'admin@admin.com', NULL, 'storage/users/February2020/profile-pics-18.jpg', '$2y$10$alaSU8OoepUgvsn9EqwfPuGljAYMVAl6UKtrq85ZqBUKyMaWqBoq.', 'KSNZBSr5QFHy7iPQ3jRx4YeUXW7WWEj93uqPaAiBKdebILn2PfWqjz5Oe0eX', '2019-10-22 18:18:52', '2020-02-13 21:53:01');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `advertisers`
--
ALTER TABLE `advertisers`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `causes`
--
ALTER TABLE `causes`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `advertisers`
--
ALTER TABLE `advertisers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `causes`
--
ALTER TABLE `causes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `comments`
--
ALTER TABLE `comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `events`
--
ALTER TABLE `events`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `menus`
--
ALTER TABLE `menus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT untuk tabel `messages`
--
ALTER TABLE `messages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT untuk tabel `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `settings`
--
ALTER TABLE `settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
