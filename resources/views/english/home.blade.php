@extends('english.layout.main')

@section('content')
<!-- ============================================================= Content Start ============================================================= -->
<!--================ start banner Area =================-->
<section class="home-banner-area relative" id="home" data-parallax="scroll"
    data-image-src="{{ asset(Setting()->HomePicture) }}">
    <div class="overlay-bg overlay"></div>
    <div class="container">
        <div class="row fullscreen justify-content-lg-begin">
            <div class="banner-content col-lg-10">
                @if ($message = Session::get('success'))
                <div class="alert alert-success" role="alert">
                    {!! $message !!}
                </div>
                <?php Session::forget('success');?>
                @endif
                @if ($message = Session::get('error'))
                <div class="alert alert-success" role="alert">
                    {!! $message !!}
                </div>
                <?php Session::forget('error');?>
                @endif
                <h1>{{ Setting()->title_home_en }} <br>children</h1>
                <h4>{!! Setting()->sub_title_home_en !!}</h4>
                <a href="{{ url('Contact')}}" class="primary-btn">Join us<i class="ti-angle-right ml-1"></i></a>
            </div>
        </div>
    </div>
    <div class="shape-bottom overflow-hidden">
        <img src="{{asset('assets/img/hero-shape.svg')}}" alt=" shape" class="bottom-shape">
    </div>
</section>
<!--================ End banner Area =================-->
<!--================ Start About Area =================-->
<section class="about_area lite_bg">
    <!-- ===============================  Home  ======================================== -->
    <style type="text/css">
        .about_area .about_bg {
            background:linear-gradient(0deg, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)),
            url('{{ asset(Setting()->AboutPicture) }}');
            background-repeat: no-repeat;
            background-size: cover;
            background-size: cover;
            position: absolute;
            left: 50%;
            top: 0;
            height: 100%;
            width: 50%;
            z-index: -1;
        }
    </style>
    <!-- ===============================  Home  ======================================== -->
    <div class="container">
        <div class="row align-items-end">
            <div class="col-lg-5">
                <div class="about_details lite_bg">
                    <h2>{!! Setting()->title_about_en !!}</h2>
                    <p class="mb-0">{!! Setting()->content_about_en !!}</p>
                    <a href="{{ url('about') }}" class="primary-btn mt-5">
                        Read more
                        <i class="ti-angle-right ml-1"></i>
                    </a>
                </div>
            </div>
            <div class="col-lg-4 offset-lg-3 col-md-6 offset-md-1 d-lg-block d-none">
                <div class="about_right">
                    <div class="video-inner justify-content-center align-items-center d-flex">
                        <a id="play-home-video" class="video-play-button" href="{!! Setting()->video !!}">
                            <span></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="about_bg d-lg-block d-none"></div>
    </div>
</section>
<!--================ End About Area =================-->

<!--================ Start Features Area =================-->
<section class="features-area section-gap-top">
    <div class="container">

        <div class="row feature_inner">
            <div class="col-lg-4 col-md-6">
                <div class="feature-item">
                    <i class="fi flaticon-compass"></i>
                    <h4>Give Donation</h4>
                    <p>{!! Setting()->content_feature_en !!}</p>
                    <a href="{{ url('Contact') }}" class="primary-btn2">Learn more</a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="feature-item">
                    <i class="fi flaticon-desk"></i>
                    <h4>Give Inspiration</h4>
                    <p>{!! Setting()->content_feature_two_en !!}</p>
                    <a href="{{ url('Contact') }}" class="primary-btn2">Learn more</a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="feature-item">
                    <i class="fi flaticon-bathroom"></i>
                    <h4>Become Volunteer</h4>
                    <p>{!! Setting()->content_feature_three_en !!}</p>
                    <a href="{{ url('Contact') }}" class="primary-btn2">Learn more</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================ End Features Area =========================-->
<!--================ Start Popular Causes Area =================-->
<section class="popular-cause-area section-gap-top">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="section-title">
                    <span> Dignity, Opportunity, Hope </span>
                    <h2><span>Our Donations</span></h2>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach($Causes as $Cause)
            <div class="col-lg-4 col-md-6">
                <div class="card single-popular-cause">
                    <div class="card-body">
                        <figure>
                            <img class="card-img-top img-fluid" src="{!! asset($Cause->image) !!}" alt="Card image cap">
                        </figure>
                        <div class="card_inner_body">
                            @if(isset($Cause->Category->title))
                            <div class="tag">{{ $Cause->Category->title }}</div>
                            @else
                            <div class="tag">No Category</div>
                            @endif
                            <h4 class="card-title">{!! substr($Cause->Title_en, 0, 190) !!}</h4>
                            <div class="d-flex justify-content-between raised_goal">
                                <p>Raised: {!! $Cause->Raised !!}</p>
                                <p><span>Goal: {!! $Cause->Goal !!}</span></p>
                            </div>
                            <div class="d-flex justify-content-between donation align-items-center">
                                <a href="{!! url('Causes') !!}/{!! $Cause->slug !!}" class="primary-btn">Donate</a>
                                <p><span class="ti-heart mr-1"></span> {!! $Cause->Donors !!} Donors</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
<!--================ End Popular Causes Area =================-->

<!--================ Start callto Area =================-->
<section class="callto-area section-gap relative">
    <div class="overlay overlay-bg"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <p class="top_text">Need your help?</p>
                <div class="call-wrap mx-auto">
                    <h1>Volunteer Needed At Your Area</h1>
                    <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of
                        classical Latin literature from 45 BC, making it over 2000 years old.</p>
                    <a href="{{ route('register') }}" class="primary-btn mt-5">
                        Sign up
                        <i class="ti-angle-right ml-1"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================ End callto Area ===========================-->
<!--================ Start Upcoming Event Area =================-->
<section class="upcoming_event_area section-gap-top">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="section-title">
                    <h2><span>Upcoming</span> Event</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- ===============================  Home Event ======================================== -->
            @foreach($Events as $Event)
            <div class="col-lg-6">
                <!-- ===============================  Home Event ======================================== -->
                <div class="single_upcoming_event">
                    <!-- ===============================  Home Event ======================================== -->
                    <div class="row align-items-center">
                        <!-- ===============================  Home Event ======================================== -->
                        <div class="col-lg-6 col-md-6">
                            <figure>
                                <img class="img-fluid w-100" src="{!! asset($Event->image) !!}" alt="">
                                <div class="date">
                                    {{ date('M j', strtotime($Event->created_at)) }}
                                </div>
                            </figure>
                        </div>
                        <!-- ===============================  Home Event ======================================== -->
                        <div class="col-lg-6 col-md-6">
                            <div class="content_wrapper">
                                <h3 class="title">
                                    <a href="{!! url('Events') !!}/{!! $Event->slug !!}">{!! substr($Event->Title_en, 0,
                                        190) !!}</a>
                                </h3>
                                <!-- ===============================  Home Event ======================================== -->
                                <p>{!! substr($Event->Content_en, 0, 60) !!}</p>
                                <!-- ===============================  Home Event ======================================== -->
                                <div class="d-flex count_time justify-content-lg-start justify-content-center"
                                    id="clockdiv1">
                                    <!-- ===============================  Home Event ======================================== -->
                                    <div class="single_time">
                                        <h4 class="days">{!! $Event->Days !!}</h4>
                                        <p>Days</p>
                                    </div>
                                    <!-- ===============================  Home Event ======================================== -->
                                    <div class="single_time">
                                        <h4 class="hours">{!! $Event->Hours !!}</h4>
                                        <p>Mounth</p>
                                    </div>
                                    <!-- ===============================  Home Event ======================================== -->
                                    <div class="single_time">
                                        <h4 class="minutes">{!! $Event->Minutes !!}</h4>
                                        <p>Year</p>
                                    </div>
                                    <!-- ===============================  Home Event ======================================== -->
                                </div>
                                <a href="{!! url('Events') !!}/{!! $Event->slug !!}" class="primary-btn2">Learn More</a>
                            </div>
                            <!-- ===============================  Home Event ======================================== -->
                        </div>
                        <!-- ===============================  Home Event ======================================== -->
                    </div>
                    <!-- ===============================  Home Event ======================================== -->
                </div>
                <!-- ===============================  Home Event ======================================== -->
            </div>
            @endforeach
            <!-- ===============================  Home Event ======================================== -->
        </div>
    </div>
</section>
<!--================ End Upcoming Event Area =================-->

<!--================ Start Home Blog Area =================-->
<section class="blog-area section-gap-top">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-12">
                <div class="home-blog-left">
                    <h2>Latest From
                        Our Blog </h2>
                    <p>
                        <!-- ===============================  Home Post ======================================== -->
                        {!! Setting()->content_blog_en !!}
                    </p>
                    <a href="{{ url('Posts') }}" class="primary-btn2">Show more</a>
                </div>
            </div>
            <!-- ===============================  Home Post ======================================== -->
            @foreach($Posts as $Post)
            <div class="col-lg-4 col-md-6 single-blog">
                <div class="thumb">

                    <img class="img-fluid" src="{!! asset($Post->image) !!}"
                        alt="{!! substr($Post->Title_en, 0, 190) !!}">
                </div>
                <div class="single-blog-info">
                    <p class="tag">
                        @if(isset($Post->Category->title))
                        <span>{{ $Post->Category->title }}</span>
                        @else
                        <span>No Category</span>
                        @endif
                        <span>{{ date('M j, Y', strtotime($Post->created_at)) }}</span>
                    </p>
                    <a href="{!! url('Posts') !!}/{!! $Post->slug !!}">
                        <h4>{!! substr($Post->Title_en, 0, 190) !!}</h4>
                    </a>
                    <div class="meta-bottom d-flex">
                        @if(isset($Post->Comments))
                        <a class="mr-3"><span class="ti-comments mr-2"></span>{{ count($Post->Comments) }} Comments</a>
                        @else
                        <a class="mr-3"><span class="ti-comments mr-2"></span> 0 Comments</a>
                        @endif
                        <a href="{!! url('Posts') !!}/{!! $Post->slug !!}"> <span class="ti-eye mr-2"></span> Show
                            more</a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
<!--================ End Home Blog Area =================-->

<!--================ Start Gallery Area =================-->
<section class="instagram-area section-gap-top">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="section-title">
                    <h2><span>Our Gallery</span></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="instagram-slider owl-carousel">
        @foreach($Gallers as $Gallery)
        <a href="{!! asset($Gallery->image) !!}" class="single-instagram d-block img-pop-up">
            <div class="instagram-img">
                <img src="{!! asset($Gallery->image) !!}">
                <div class="instagram-text"><i class=" icon-material-outline-add"></i></div>
            </div>
        </a>
        @endforeach
    </div>
</section>
<!--================ End Gallery Area =================-->
<!-- ============================================================= Content end   ============================================================= -->
@endsection
