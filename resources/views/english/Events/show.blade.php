@extends('english.layout.main')

@section('content')
<!-- ============================================================= Content Start ============================================================= -->
<style type="text/css">
    .banner-area {
        background:url('{{ asset($Event->image) }}') no-repeat;
        background-size: cover
    }
</style>
<section class="banner-area relative">
    <div class="overlay overlay-bg"></div>
    <div class="container">
        <div class="row align-items-center banner-content">
            <div class="col-lg-8">
                <!--================ Start Popular Event Area =================-->
                <h1 class="text-white">{!! $Event->Title_en !!}</h1>
            </div>
        </div>
    </div>
    <div class="shape-bottom overflow-hidden">
        <img src="{{asset('assets/img/hero-shape.svg')}}" alt=" shape" class="bottom-shape">
    </div>
</section>
<!--================ End top-section Area =================-->

<!--================ Start Recent Event Area =================-->
<section class="event_details section-gap-top">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <!--================ Start Popular Event Area =================-->
                <img src="{{ asset($Event->image) }}" alt="" class="img-fluid">
            </div>
        </div>
        <div class="event_content mb-40">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="right_content">
                        <!--================ Start Popular Event Area =================-->
                        <h2>{!! $Event->Title_en !!}</h2>
                        <p>{!! $Event->Content_en !!}</p>
                    </div>
                </div>
                <div class="col-lg-4 mt-lg-0 mt-5">
                    <div class="left_content">
                        <div class="mb-40">
                            <h5>
                                <i class="icon-line-awesome-hourglass-2"></i>
                                Days Event Do
                            </h5>
                            <div style="display: flex;">
                                <div class="ml-30">{!! $Event->Days !!}</div>
                                <div class="ml-10">/{!! $Event->Hours !!}</div>
                                <div class="ml-10">/{!! $Event->Minutes !!}</div>
                            </div>
                        </div>

                        <div class="mb-40">
                            <h5>
                                <i class="icon-material-outline-add-location"></i>
                                Address
                            </h5>
                            <!--================ Start Popular Event Area =================-->
                            <div class="ml-30">{!! $Event->Address !!}</div>
                        </div>

                        <div class="">
                            <h5>
                                <i class=" icon-material-outline-access-time"></i>
                                FinishTime & StartTime
                            </h5>
                            <div style="display: flex;">
                                <div class="ml-30">{!! $Event->FinishTime !!}</div>
                                <div class="ml-10">-</div>
                                <div class="ml-10">{!! $Event->StartTime !!}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================ End Recent Event Area =================-->
<!--================ Start CTA Area =================-->
<div class="cta-area ">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <p class="top_text">Thanks for your heart.</p>
                <h1>Contribute on my charity work by your donation. Thanks for your heart.</h1>
                <!--================ Start Popular Event Area =================-->
                <a href="{!! url('Contact') !!}" class="primary-btn">donation</a>
            </div>
        </div>
    </div>
</div>
<!--================ End CTA Area =================-->
<!-- ============================================================= Content end   ============================================================= -->
@endsection
